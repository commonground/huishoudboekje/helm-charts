# Logservice DB migration to 2.0.0
Includes check to make sure DB secret is correct. Must be executed AFTER the v2.0.0 update.

**Warning: Rollbacks not supported!** 

Only compatible with Huishoudboekje installations managed by the Helm charts from this repository.

## Example usage:
To install and run the job (assuming the GitLab Helm repository is present under the name "huishoudboekje"):
`helm install logsvc-db-migration huishoudboekje/logservice-db-migration --atomic --wait-for-jobs --set hhbReleaseName=hhb-sloothuizen-acc`

To uninstall when done:
`helm uninstall almsvc-db-migration`

