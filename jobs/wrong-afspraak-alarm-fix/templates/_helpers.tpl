{{/*
Expand the name of the chart.
*/}}
{{- define "wrong-afpsraak-alarm-fix.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "wrong-afpsraak-alarm-fix.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "wrong-afpsraak-alarm-fix.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "wrong-afpsraak-alarm-fix.labels" -}}
helm.sh/chart: {{ include "wrong-afpsraak-alarm-fix.chart" . }}
{{ include "wrong-afpsraak-alarm-fix.selectorLabels" . }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "wrong-afpsraak-alarm-fix.selectorLabels" -}}
app.kubernetes.io/name: {{ include "wrong-afpsraak-alarm-fix.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}
