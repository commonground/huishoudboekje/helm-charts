# HHB script to fix missing end dates for alarms

Script to fix historic data after the bug causing this was fixed in HHB v2.2.3.

https://gitlab.com/commonground/huishoudboekje/huishoudboekje-migrationscripts/-/blob/a1d7e58a86214281158c6472ca41a751c9560287/Fix_MissingEndDateAlarms/main.py

**Warning: Rollbacks not supported!** 

Only compatible with Huishoudboekje installations managed by the Helm charts from this repository.

## Example usage:
To install and run the job (assuming the GitLab Helm repository is present under the name "huishoudboekje"):
`helm install fix-missing-end-date-alarms huishoudboekje/fix-missing-end-date-alarms --atomic --wait-for-jobs --set hhbReleaseName=hhb-sloothuizen-acc`

To uninstall when done:
`helm uninstall fix-missing-end-date-alarms`
