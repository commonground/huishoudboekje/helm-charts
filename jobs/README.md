# One-time jobs

This directory contains a set of scripts and jobs meant to be executed only once. This can for example be fixes in data
or one-time DB migrations.

All of them are packaged as Helm charts and can be installed just like the main application. All assume Huishoudboekje
is installed using the Helm charts in this repository.
