# HHB v2.0 to v2.1.4 DB migration
Includes check to make sure DB secret is correct. Must be executed AFTER the v2.0.0 update but before the v2.1.4 update.
For HHB v2.1.x to v2.1.4, see the hhb2-1-to-2-1-4 job.

**Warning: Rollbacks not supported!** 

Only compatible with Huishoudboekje installations managed by the Helm charts from this repository.

## Example usage:
To install and run the job (assuming the GitLab Helm repository is present under the name "huishoudboekje"):
`helm install hhb2-0-to-2-1-4-migration huishoudboekje/hhb2-0-to-2-1-4-migration --atomic --wait-for-jobs --set hhbReleaseName=hhb-sloothuizen-acc`

To uninstall when done:
`helm uninstall hhb2-0-to-2-1-4-migration`
