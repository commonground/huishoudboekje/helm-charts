# Helm charts for Huishoudboekje

## Using the Helm chart
This folder is a Helm chart functioning as umbrella chart for the sub charts in the [Charts](./charts) folder which
represent the services of the application.

All services are technically optional (all sub charts can be enabled or disabled) however all but KeyCloak and
conditionally the database are required for the application to function.

Since version 1.9.9 the application supports StatsD metrics. These Helm charts implement this functionality too and can
optionally install a StatsD to Prometheus metrics sidecar container to expose the metrics as Prometheus-compatible.
If both are enabled, the URL to fetch metrics for each Python service would be
`<release name>-<service name>:9102/metrics`.

Below is a chart defining which version of Huishoudboekje is compatible with which Helm chart version(s). Values
compatibility is shown for recommended Helm chart version.

| Huishoudboekje version | Recommended Helm chart version | Values compatibility |
|:-----------------------|:-------------------------------|:---------------------|
| 2.3.0                  | 5.1.2                          | ≥ 5.0.0              |
| 2.2.3                  | 5.0.0                          | ≥ 5.0.0              |


<details><summary>Legacy versions of Huishoudboekje and the Huishuidboekje Helm charts.</summary>

| Huishoudboekje version | Recommended Helm chart version | Values compatibility   |
|:-----------------------|:-------------------------------|:-----------------------|
| 2.2.2                  | 4.3.5                          | ≥ 4.3.3 and ≤ 4.3.5    |
| 2.2.1                  | 4.3.5                          | ≥ 4.3.3                |
| 2.2.0                  | 4.3.2                          | ≥ 4.3.1 and ≤ 4.3.2    |
| 2.1.6                  | 4.3.0                          | ≥ 4.0.0 and ≤ 4.3.0    |
| 2.1.5                  | 4.3.0                          | ≥ 4.0.0 and ≤ 4.3.0    |
| 2.1.4                  | 4.3.0                          | ≥ 4.0.0 and ≤ 4.3.0    |
| 2.1.3                  | 4.3.0                          | ≥ 4.0.0 and ≤ 4.3.0    |
| 2.1.2                  | 4.3.0                          | ≥ 4.0.0 and ≤ 4.3.0    |
| 2.1.1                  | 4.3.0                          | ≥ 4.0.0 and ≤ 4.3.0    |
| 2.1.0                  | 4.3.0                          | ≥ 4.0.0 and ≤ 4.3.0    |
| 2.0.6                  | 4.1.0                          | ≥ 4.0.0 and ≤ 4.3.0    |
| 2.0.5                  | 4.1.0                          | ≥ 4.0.0 and ≤ 4.3.0    |
| 2.0.4                  | 4.1.0                          | ≥ 4.0.0 and ≤ 4.3.0    |
| 2.0.3                  | 4.1.0                          | ≥ 4.0.0 and ≤ 4.3.0    |
| 2.0.2                  | 4.1.0                          | ≥ 4.0.0 and ≤ 4.3.0    |
| 2.0.1                  | 4.1.0                          | ≥ 4.0.0 and ≤ 4.3.0    |
| 2.0.0                  | None, incompatible             | N/A                    |
| 1.12.7                 | 3.4.3                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.12.6                 | 3.4.3                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.12.5                 | 3.4.3                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.12.4                 | 3.4.3                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.12.3                 | 3.4.3                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.12.2                 | 3.4.3                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.12.1                 | 3.4.3                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.12.0                 | 3.4.3                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.11.0                 | 3.3.2                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.10.12                | 3.2.0                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.10.11                | 3.2.0                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.10.10                | 3.2.0                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.10.9                 | 3.2.0                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.10.7                 | 3.2.0                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.10.6                 | 3.2.0                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.10.5                 | 3.2.0                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.10.4                 | 3.2.0                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.10.3                 | 3.2.0                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.10.2                 | 3.2.0                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.10.1                 | 3.2.0                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.10.0                 | 3.2.0                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.9.11                 | 3.2.0                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.9.10                 | 3.0.3                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.9.9                  | 3.0.3                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.9.8                  | 3.0.3                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.9.7                  | 3.0.3                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.9.6                  | 3.0.3                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.9.5                  | 3.0.3                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.9.4                  | 3.0.3                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.9.3                  | 3.0.3                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.9.2                  | 3.0.3                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.9.1                  | 3.0.3                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.9.0                  | 3.0.3                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.8.4                  | 3.0.3                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.8.3                  | 3.0.3                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.8.2                  | 3.0.3                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.8.1                  | 3.0.3                          | ≥ 3.0.0 and ≤ 3.4.3    |
| 1.8.0                  | ≥ 2.1.0                        | ≥ 1.4.0 and ≤ 2.1.1    |
| 1.7.0                  | ≥ 2.1.0                        | ≥ 1.4.0 and ≤ 2.1.1    |
| 1.6.0                  | ≥ 2.1.0                        | ≥ 1.4.0 and ≤ 2.1.1    |
| 1.5.0                  | ≥ 2.1.0                        | ≥ 1.4.0 and ≤ 2.1.1    |
| 1.4.0                  | ≥ 2.1.0                        | ≥ 1.4.0 and ≤ 2.1.1    |
| 1.3.0                  | = 1.3.0-1                      | ≥ 1.2.0 and ≤ 1.3.0-1  |
| 1.2.3                  | = 1.2.3                        | ≥ 1.2.0 and ≤ 1.3.0-1  |
| 1.2.2                  | = 1.2.1-1                      | ≥ 1.2.0 and ≤ 1.3.0-1  |
| 1.2.1                  | = 1.2.1-1                      | ≥ 1.2.0 and ≤ 1.3.0-1  |
| 1.2.0                  | = 1.2.0                        | ≥ 1.2.0 and ≤ 1.3.0-1  |
| 1.1.2                  | = 1.0.0                        | = 1.0.0                |
| 1.1.1                  | = 1.0.0                        | = 1.0.0                |
| 1.0.0                  | = 1.0.0                        | = 1.0.0                |
</details>

By default, each Helm chart version installs the Huishoudboekje version which was the most recent at the time of the
chart version release. The to be installed HHB version can be overridden using the value `global.image.tag`.

## Database
Huishoudboekje currently only officially supports PostgreSQL. This can be installed with Huishoudboekje as part of this
Helm chart, or an external PostgreSQL server can be used. This choice applies to all sub charts.

As a side note, it has been reported EnterpriseDB (based on PostgreSQL) also works, however it is not officially
supported.

Huishoudboekje also requires a Redis server to store authentication information and RabbitMQ as message queue system.
Both are included in the Helm charts but it is possible to specify an external Redis and/or RabbitMQ server to use.

### External DB
The following services require a database and connection settings:

* alarmenservice
* grootboekservice
* fileservice
* huishoudboekjeservice
* logservice
* keycloak (optional, off by default)
* organisatieservice
* partyservice
* postadressenservice

The accounts need full rights on their respective databases (`GRANT ALL PRIVILEGES`). This may be one account for all
services, but it is also possible to define a different server, port, database name, user and password for each service.

See also [default global values](values.yaml).

## Metrics collection
Since version 1.9.9 Huishoudboekje supports StatsD metrics and since version 2.0.0 the Alarmenservice and Logservice
support native Prometheus metrics.

Using Helm values, the host to which StatsD statistics may be exported can be configured. Alternatively, included with
this chart is an optional Prometheus exporter. Enabling this will configure the Huishoudboekje services which support it
to export StatsD metrics to a sidecar container which in turn will make these available in Prometheus format. The Helm
chart can also install Prometheus ServiceMonitor resources to automatically configure a Prometheus Operator if it is
available in the cluster. The chart install/upgrade will fail and throw an error if this enabled but not supported by
the cluster. Metrics are disabled by default.

Only the Python-based services support StatsD metrics. Prometheus metrics, if enabled, can be scraped from the services
on port `9102` and path `/metrics`. The Alarmenservice, Banktransactieservice, Fileservice and Logservice support
Prometheus metrics natively and can make them available on port `9000`, also on path `/metrics`.

## Versioning
The charts follow [SemVer 2.0.0](https://semver.org/). Each sub chart is separately versioned. The parent chart's
version is increased according to the highest change level of the sub chart(s): if one service was bumped from 1.0.0 to
1.0.1 (patch level increase) and another from 1.0.0 to 1.1.0 (minor level increase), the parent chart will also have a
minor level increase.

Up to version 1.4.0 of Huishoudboekje, the Helm charts followed the Huishoudboekje versions. Since Helm chart version
2.0.0 and Huishoudboekje 1.7.0 this is no longer the case. To prevent version downgrades, with this change, the Helm
chart version has been bumped to 2.0.0.

## Known Issues/Limitations
* The DB timestamp method works but could be done better, perhaps.
* Updating the KeyCloak admin credentials only re-runs the setup job but does not actually update the KeyCloak admin
  account, causing the setup job to fail and the KeyCloak admin credentials to stop matching the configured ones.
  *POSSIBLE FIX*: Determine if username or password changed using Helm lookup functionality. If so, connect to DB and
  update credentials.
* It is currently not possible to perform a Helm upgrade with a new DNS domain if OpenShift Route CRD objects are used,
  which is the default if the API supports this object.
