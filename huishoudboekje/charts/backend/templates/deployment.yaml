---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "backend.fullname" . }}
  labels:
    {{- include "backend.labels" . | nindent 4 }}
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "backend.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        {{- include "backend.selectorLabels" . | nindent 8 }}
    spec:
      enableServiceLinks: false
      automountServiceAccountToken: false
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ coalesce .Values.image.tag .Values.global.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: "{{ coalesce .Values.image.pullPolicy .Values.global.image.pullPolicy }}"
          ports:
            - name: http
              containerPort: 8000
              protocol: TCP
          startupProbe:
            httpGet:
              path: /api/health
              port: http
            periodSeconds: 10
            failureThreshold: 60
          livenessProbe:
            httpGet:
              path: /api/health
              port: http
            periodSeconds: 10
          readinessProbe:
            httpGet:
              path: /api/health
              port: http
            periodSeconds: 10
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
          env:
            - name: PREFIX
              value: "/api"
            - name: USE_GRAPHIQL
              value: {{ .Values.useGraphiql | quote }}
            {{- with .Values.global }}
            {{- if .alarmenservice.enabled }}
            - name: ALARMENSERVICE_URL
              value: {{ printf "http://%s-%s" $.Release.Name "alarmenservice" | quote }}
            {{- end }}{{- if .banktransactieservice.enabled }}
            - name: TRANSACTIE_SERVICE_URL
              value: {{ printf "http://%s-%s" $.Release.Name "banktransactieservice"  | quote }}
            {{- end }}{{- if .grootboekservice.enabled }}
            - name: GROOTBOEK_SERVICE_URL
              value: {{ printf "http://%s-%s" $.Release.Name "grootboekservice" | quote }}
            {{- end }}{{- if .huishoudboekjeservice.enabled }}
            - name: HHB_SERVICE_URL
              value: {{ printf "http://%s-%s" $.Release.Name "huishoudboekjeservice" | quote }}
            {{- end }}{{- if .logservice.enabled }}
            - name: LOG_SERVICE_URL
              value: {{ printf "http://%s-%s" $.Release.Name "logservice" | quote }}
            {{- end }}{{- if .organisatieservice.enabled }}
            - name: ORGANISATIE_SERVICE_URL
              value: {{ printf "http://%s-%s" $.Release.Name "organisatieservice" | quote }}
            {{- end }}{{- if .postadressenservice.enabled }}
            - name: POSTADRESSENSERVICE_URL
              value: {{ printf "http://%s-%s" $.Release.Name "postadressenservice" | quote }}
            {{- end }}{{- if .rapportageservice.enabled }}
            - name: RAPPORTAGE_SERVICE_URL
              value: {{ printf "http://%s-%s" $.Release.Name "rapportageservice" | quote }}
            {{- end }}{{- end }}
            - name: JWT_SECRET
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-jwt" .Release.Name }}
                  key: secret
            - name: JWT_AUDIENCE
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-jwt" .Release.Name }}
                  key: audience
            - name: JWT_ISSUER
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-jwt" .Release.Name }}
                  key: issuer
            - name: JWT_ALGORITHMS
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-jwt" .Release.Name }}
                  key: algorithms
            - name: LOG_LEVEL
              value: {{ coalesce .Values.log_level .Values.global.log_level | upper | quote }}
            {{- if .Values.internalConnectionTimeout }}
            - name: INTERNAL_CONNECTION_TIMEOUT
              value: {{ .Values.internalConnectionTimeout }}
            {{- end }}
            {{- if .Values.internalReadTimeout }}
            - name: INTERNAL_READ_TIMEOUT
              value: {{ .Values.internalReadTimeout }}
            {{- end }}
            {{- if .Values.global.metrics.enabled }}
            - name: STATSD_HOST
              value: {{ .Values.global.metrics.host }}
            {{- end }}
            - name: ALLOW_INTROSPECTION
              value: {{ .Values.enableGraphqlIntrospection | ternary "1" "0" | quote }}
            - name: GUNICORN_WORKER_TIMEOUT
              value: "0"
            {{- if .Values.global.rabbitmq.enabled }}
            - name: RABBITMQ_HOST
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-rabbitmq" .Release.Name }}
                  key: host
            - name: RABBITMQ_PORT
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-rabbitmq" .Release.Name }}
                  key: port
            - name: RABBITMQ_USER
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-rabbitmq" .Release.Name }}
                  key: username
            - name: RABBITMQ_PASS
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-rabbitmq" .Release.Name }}
                  key: password
            {{- end }}
            {{- if .Values.extraEnvVars }}{{- toYaml .Values.extraEnvVars | nindent 12 }}{{- end }}
          volumeMounts:
            - mountPath: /var/tmp
              name: writable-tmp
        {{- if and .Values.global.metrics.enabled .Values.global.metrics.prometheusExporter.enabled }}
        - name: statsd-prometheus-exporter
          image: "{{ .Values.global.metrics.prometheusExporter.image.repository }}:{{ .Values.global.metrics.prometheusExporter.image.tag }}"
          imagePullPolicy: "{{ coalesce .Values.image.pullPolicy .Values.global.image.pullPolicy }}"
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          ports:
            - name: metrics
              containerPort: 9102
              protocol: TCP
        {{- end }}
      volumes:
        - name: writable-tmp
          emptyDir: {}
