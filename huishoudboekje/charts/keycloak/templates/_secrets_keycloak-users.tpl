{{/*
Generate templates for KeyCloak admin authentication.
*/}}

{{/*
Template function to lookup a value in a user provided secret and return it.
*/}}
{{- define "_getExistingUsersSecretValue" -}}
{{- $resource := ((lookup "v1" "Secret" .context.Release.Namespace .context.Values.existingSecrets.users.name).data | default dict) -}}
{{- printf "%s" (get $resource .info | b64dec) -}}
{{- end -}}

{{/*
Fetch data from existing secret if it exists which is expected to be valid JSON. If empty, fetch data from values, which
is expected to be valid YAML and will be converted to JSON. Returns JSON as string object.
*/}}
{{- define "keycloak.users" -}}
  {{- coalesce (include "_getExistingUsersSecretValue" (dict "context" . "info" .Values.existingSecrets.users.json)) (.Values.users | mustToJson) -}}
{{- end -}}
