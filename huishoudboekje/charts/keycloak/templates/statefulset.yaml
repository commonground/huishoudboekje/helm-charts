---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ include "keycloak.fullname" . }}
  labels:
    {{- include "keycloak.labels" . | nindent 4 }}
spec:
  serviceName: {{ printf "%s-headless" (include "keycloak.fullname" .) | trunc 63 | trimSuffix "-" }}
  selector:
    matchLabels:
      {{- include "keycloak.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        {{- include "keycloak.selectorLabels" . | nindent 8 }}
      annotations:
        checksum/secrets: {{ include (print $.Template.BasePath "/secrets.yaml") . | sha256sum }}
    spec:
      enableServiceLinks: false
      automountServiceAccountToken: false
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      {{- if .Values.global.database.enabled }}
      initContainers:
        - name: {{ .Chart.Name }}-wait-for-db
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: busybox
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          env:
            - name: POSTGRESQL_HOSTNAME
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-keycloak-database" .Release.Name }}
                  key: POSTGRESQL_HOST
            - name: POSTGRESQL_PORT
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-keycloak-database" .Release.Name }}
                  key: POSTGRESQL_PORT
          command:
            - "sh"
            - "-c"
            - "while true; do sleep 1; echo Waiting for database...; if nc -z $POSTGRESQL_HOSTNAME $POSTGRESQL_PORT; then echo Database is up!; exit 0; fi; done; echo Aint gonna wait for the database forever...; exit 1"
        - name: {{ .Chart.Name }}-prepare-db
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: bitnami/postgresql:13
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          env:
            - name: POSTGRESQL_HOSTNAME
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-keycloak-database" .Release.Name }}
                  key: POSTGRESQL_HOST
            - name: POSTGRESQL_PORT
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-keycloak-database" .Release.Name }}
                  key: POSTGRESQL_PORT
            - name: DB_USER
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-keycloak-database" .Release.Name }}
                  key: POSTGRESQL_USERNAME
            - name: POSTGRESQL_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-database" .Release.Name }}
                  key: POSTGRES_PASSWORD
            - name: KEYCLOAK_DB_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-keycloak-database" .Release.Name }}
                  key: POSTGRESQL_PASSWORD
            - name: DB_DATABASE
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-keycloak-database" .Release.Name }}
                  key: POSTGRESQL_DATABASE_NAME
          command: [ /bin/sh ]
          args:
            - -c
            - >-
              export PGPASSWORD=$POSTGRESQL_PASSWORD &&
              export PGPORT=$POSTGRESQL_PORT &&
              psql -h $POSTGRESQL_HOSTNAME -U postgres -c "CREATE USER $DB_USER WITH PASSWORD '$KEYCLOAK_DB_PASSWORD'" || true &&
              psql -h $POSTGRESQL_HOSTNAME -U postgres -c "CREATE DATABASE $DB_DATABASE" || true &&
              psql -h $POSTGRESQL_HOSTNAME -U postgres -c "GRANT ALL PRIVILEGES ON DATABASE $DB_DATABASE TO $DB_USER" || true &&
              psql -h $POSTGRESQL_HOSTNAME -U postgres -c "ALTER USER $DB_USER WITH PASSWORD '$KEYCLOAK_DB_PASSWORD'" || true;
      {{- end }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: {{ .Values.image.repository }}:{{ .Values.image.tag }}
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          args: [start, --auto-build]
          ports:
            - name: http
              containerPort: 8080
              protocol: TCP
          startupProbe:
            httpGet:
              port: http
              path: /kc/health/ready
            periodSeconds: 30
          livenessProbe:
            httpGet:
              path: /kc/health/live
              port: http
            periodSeconds: 10
          readinessProbe:
            httpGet:
              path: /kc/health/ready
              port: http
            periodSeconds: 10
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
          env:
            - name: KC_DB
              value: "postgres"
            - name: KC_DB_URL
              valueFrom:
                secretKeyRef:
                  key: JDBC_URL
                  name: {{ printf "%s-keycloak-database" .Release.Name }}
            - name: KC_DB_USERNAME
              valueFrom:
                secretKeyRef:
                  key: POSTGRESQL_USERNAME
                  name: {{ printf "%s-keycloak-database" .Release.Name }}
            - name: KC_DB_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: POSTGRESQL_PASSWORD
                  name: {{ printf "%s-keycloak-database" .Release.Name }}
            - name: KC_HOSTNAME
              value: {{ .Values.global.dns_domain | quote }}
            - name: KC_HOSTNAME_PATH
              value: "/kc"
            - name: KC_HTTP_ENABLED
              value: "true"
            - name: KC_HTTP_RELATIVE_PATH
              value: "/kc"
            - name: KC_HEALTH_ENABLED
              value: "true"
            - name: KC_PROXY
              value: "edge"
            - name: KC_LOG_LEVEL
              value: {{ .Values.kc_log_level | quote }}
            - name: KEYCLOAK_ADMIN
              valueFrom:
                secretKeyRef:
                  key: username
                  name: {{ printf "%s-keycloak-auth" .Release.Name }}
            - name: KEYCLOAK_ADMIN_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: password
                  name: {{ printf "%s-keycloak-auth" .Release.Name }}
            - name: KEYCLOAK_ENABLE_STATISTICS
              value: "true"
            - name: KEYCLOAK_PROXY
              value: "edge"
            {{- if .Values.extraEnvVars }}{{- toYaml .Values.extraEnvVars | nindent 12 }}{{- end }}
