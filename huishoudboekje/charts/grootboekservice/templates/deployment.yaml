---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "grootboekservice.fullname" . }}
  labels:
    {{- include "grootboekservice.labels" . | nindent 4 }}
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "grootboekservice.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        {{- include "grootboekservice.selectorLabels" . | nindent 8 }}
      annotations:
        checksum/secrets: {{ include (print $.Template.BasePath "/secrets.yaml") . | sha256sum }}
    spec:
      enableServiceLinks: false
      automountServiceAccountToken: false
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      {{- if .Values.global.database.enabled }}
      initContainers:
        - name: {{ .Chart.Name }}-wait-for-db
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.init1.image.repository }}:{{ coalesce .Values.init1.image.tag }}"
          imagePullPolicy: "{{ coalesce .Values.init1.image.pullPolicy .Values.global.image.pullPolicy }}"
          env:
            - name: POSTGRESQL_HOSTNAME
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-grootboekservice-database" .Release.Name }}
                  key: POSTGRESQL_HOST
            - name: POSTGRESQL_PORT
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-grootboekservice-database" .Release.Name }}
                  key: POSTGRESQL_PORT
          command:
            - "sh"
            - "-c"
            - "while true; do sleep 1; echo Waiting for database...; if nc -z $POSTGRESQL_HOSTNAME $POSTGRESQL_PORT; then echo Database is up!; exit 0; fi; done; echo Aint gonna wait for the database forever...; exit 1"
        - name: {{ .Chart.Name }}-prepare-db
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.init2.image.repository }}:{{ coalesce .Values.init2.image.tag }}"
          imagePullPolicy: "{{ coalesce .Values.init2.image.pullPolicy .Values.global.image.pullPolicy }}"
          env:
            - name: POSTGRESQL_HOSTNAME
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-grootboekservice-database" .Release.Name }}
                  key: POSTGRESQL_HOST
            - name: POSTGRESQL_PORT
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-grootboekservice-database" .Release.Name }}
                  key: POSTGRESQL_PORT
            - name: POSTGRESQL_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-database" .Release.Name }}
                  key: POSTGRES_PASSWORD
            - name: POSTGRESQL_USERNAME_GRBSVC
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-grootboekservice-database" .Release.Name }}
                  key: POSTGRESQL_USERNAME
            - name: POSTGRESQL_PASSWORD_GRBSVC
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-grootboekservice-database" .Release.Name }}
                  key: POSTGRESQL_PASSWORD
            - name: POSTGRESQL_DATABASE_NAME_GRBSVC
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-grootboekservice-database" .Release.Name }}
                  key: POSTGRESQL_DATABASE_NAME
          command: [ /bin/sh ]
          args:
            - -c
            - >-
              export PGPASSWORD=$POSTGRESQL_PASSWORD &&
              export PGPORT=$POSTGRESQL_PORT &&
              psql -h $POSTGRESQL_HOSTNAME -U postgres -c "CREATE USER $POSTGRESQL_USERNAME_GRBSVC WITH PASSWORD '$POSTGRESQL_PASSWORD_GRBSVC'" || true &&
              psql -h $POSTGRESQL_HOSTNAME -U postgres -c "CREATE DATABASE $POSTGRESQL_DATABASE_NAME_GRBSVC" || true &&
              psql -h $POSTGRESQL_HOSTNAME -U postgres -c "GRANT ALL PRIVILEGES ON DATABASE $POSTGRESQL_DATABASE_NAME_GRBSVC TO $POSTGRESQL_USERNAME_GRBSVC" || true &&
              psql -h $POSTGRESQL_HOSTNAME -U postgres -c "ALTER USER $POSTGRESQL_USERNAME_GRBSVC WITH PASSWORD '$POSTGRESQL_PASSWORD_GRBSVC'" || true;
        - name: {{ .Chart.Name }}-migrate-db
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ coalesce .Values.image.tag .Values.global.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: "{{ coalesce .Values.image.pullPolicy .Values.global.image.pullPolicy }}"
          command: [ flask ]
          args: [ db, upgrade ]
          env:
            - name: DATABASE_URL
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-grootboekservice-database" .Release.Name }}
                  key: POSTGRESQL_URL_LEGACY
      {{- end }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ coalesce .Values.image.tag .Values.global.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: "{{ coalesce .Values.image.pullPolicy .Values.global.image.pullPolicy }}"
          ports:
            - name: http
              containerPort: 8000
              protocol: TCP
          startupProbe:
            httpGet:
              path: /health
              port: http
            periodSeconds: 10
            failureThreshold: 60
          livenessProbe:
            httpGet:
              path: /health
              port: http
            periodSeconds: 10
          readinessProbe:
            httpGet:
              path: /health
              port: http
            periodSeconds: 10
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
          env:
            - name: DATABASE_URL
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-grootboekservice-database" .Release.Name }}
                  key: POSTGRESQL_URL_LEGACY
            - name: LOG_LEVEL
              value: {{ coalesce .Values.log_level .Values.global.log_level | upper | quote }}
            {{- if .Values.global.metrics.enabled }}
            - name: STATSD_HOST
              value: {{ .Values.global.metrics.host }}
            {{- end }}
            {{- if .Values.extraEnvVars }}{{- toYaml .Values.extraEnvVars | nindent 12 }}{{- end }}
            - name: GUNICORN_WORKER_TIMEOUT
              value: "0"
        {{- if and .Values.global.metrics.enabled .Values.global.metrics.prometheusExporter.enabled }}
        - name: statsd-prometheus-exporter
          image: "{{ .Values.global.metrics.prometheusExporter.image.repository }}:{{ .Values.global.metrics.prometheusExporter.image.tag }}"
          imagePullPolicy: "{{ coalesce .Values.image.pullPolicy .Values.global.image.pullPolicy }}"
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          ports:
            - name: metrics
              containerPort: 9102
              protocol: TCP
        {{- end }}
