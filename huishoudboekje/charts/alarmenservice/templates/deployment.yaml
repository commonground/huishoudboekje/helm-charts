---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "alarmenservice.fullname" . }}
  labels:
    {{- include "alarmenservice.labels" . | nindent 4 }}
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "alarmenservice.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        {{- include "alarmenservice.selectorLabels" . | nindent 8 }}
      annotations:
        checksum/secrets: {{ include (print $.Template.BasePath "/secrets.yaml") . | sha256sum }}
    spec:
      enableServiceLinks: false
      automountServiceAccountToken: false
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      {{- if .Values.global.database.enabled }}
      initContainers:
        - name: {{ .Chart.Name }}-wait-for-db
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.init1.image.repository }}:{{ coalesce .Values.init1.image.tag }}"
          imagePullPolicy: "{{ coalesce .Values.init1.image.pullPolicy .Values.global.image.pullPolicy }}"
          env:
            - name: POSTGRESQL_HOSTNAME
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-alarmenservice-database" .Release.Name }}
                  key: POSTGRESQL_HOST
            - name: POSTGRESQL_PORT
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-alarmenservice-database" .Release.Name }}
                  key: POSTGRESQL_PORT
          command:
            - "sh"
            - "-c"
            - "while true; do sleep 1; echo Waiting for database...; if nc -z $POSTGRESQL_HOSTNAME $POSTGRESQL_PORT; then echo Database is up!; exit 0; fi; done; echo Aint gonna wait for the database forever...; exit 1"
        - name: {{ .Chart.Name }}-prepare-db
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.init2.image.repository }}:{{ coalesce .Values.init2.image.tag }}"
          imagePullPolicy: "{{ coalesce .Values.init2.image.pullPolicy .Values.global.image.pullPolicy }}"
          env:
            - name: POSTGRESQL_HOSTNAME
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-alarmenservice-database" .Release.Name }}
                  key: POSTGRESQL_HOST
            - name: POSTGRESQL_PORT
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-alarmenservice-database" .Release.Name }}
                  key: POSTGRESQL_PORT
            - name: POSTGRESQL_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-database" .Release.Name }}
                  key: POSTGRES_PASSWORD
            - name: POSTGRESQL_USERNAME_ALMSVC
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-alarmenservice-database" .Release.Name }}
                  key: POSTGRESQL_USERNAME
            - name: POSTGRESQL_PASSWORD_ALMSVC
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-alarmenservice-database" .Release.Name }}
                  key: POSTGRESQL_PASSWORD
            - name: POSTGRESQL_DATABASE_NAME_ALMSVC
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-alarmenservice-database" .Release.Name }}
                  key: POSTGRESQL_DATABASE_NAME
          command: [ /bin/sh ]
          args:
            - -c
            - >-
              export PGPASSWORD=$POSTGRESQL_PASSWORD &&
              export PGPORT=$POSTGRESQL_PORT &&
              psql -h $POSTGRESQL_HOSTNAME -U postgres -c "CREATE USER $POSTGRESQL_USERNAME_ALMSVC WITH PASSWORD '$POSTGRESQL_PASSWORD_ALMSVC'" || true &&
              psql -h $POSTGRESQL_HOSTNAME -U postgres -c "CREATE DATABASE $POSTGRESQL_DATABASE_NAME_ALMSVC" || true &&
              psql -h $POSTGRESQL_HOSTNAME -U postgres -c "GRANT ALL PRIVILEGES ON DATABASE $POSTGRESQL_DATABASE_NAME_ALMSVC TO $POSTGRESQL_USERNAME_ALMSVC" || true &&
              psql -h $POSTGRESQL_HOSTNAME -U postgres -c "ALTER USER $POSTGRESQL_USERNAME_ALMSVC WITH PASSWORD '$POSTGRESQL_PASSWORD_ALMSVC'" || true;
        - name: {{ .Chart.Name }}-migrate-db
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ coalesce .Values.image.tag .Values.global.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: "{{ coalesce .Values.image.pullPolicy .Values.global.image.pullPolicy }}"
          command: [ sh ]
          args: [ /app/execute-migrations.sh ]
          env:
            - name: HHB_DATABASE_URL
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-alarmenservice-database" .Release.Name }}
                  key: POSTGRESQL_URL
      {{- end }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ coalesce .Values.image.tag .Values.global.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: "{{ coalesce .Values.image.pullPolicy .Values.global.image.pullPolicy }}"
          ports:
            - name: grpc
              containerPort: 8000
              protocol: TCP
            {{- if .Values.global.metrics.enabled }}
            - name: metrics
              containerPort: 9000
              protocol: TCP
            {{- end }}
          startupProbe:
            grpc:
              port: 8000
            periodSeconds: 10
            failureThreshold: 60
          livenessProbe:
            grpc:
              port: 8000
            periodSeconds: 10
          readinessProbe:
            grpc:
              port: 8000
            periodSeconds: 10
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
          env:
            - name: Logging__LogLevel__Default
              value: {{ include "getDotNetLogLevel" (coalesce .Values.log_level .Values.global.log_level | upper) }}
            - name: Logging__LogLevel__Microsoft.AspNetCore
              value: {{ include "getDotNetLogLevel" (coalesce .Values.log_level .Values.global.log_level | upper) }}
            - name: Logging__LogLevel__Grpc
              value: {{ include "getDotNetLogLevel" (coalesce .Values.log_level .Values.global.log_level | upper) }}
            - name: HHB_DATABASE_URL
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-alarmenservice-database" .Release.Name }}
                  key: POSTGRESQL_URL
            - name: HHB_JWT_ISSUER
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-jwt" .Release.Name }}
                  key: issuer
            - name: HHB_JWT_AUDIENCE
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-jwt" .Release.Name }}
                  key: audience
            - name: HHB_JWT_SECRET
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-jwt" .Release.Name }}
                  key: secret
            - name: HHB_JWT_ALLOWED_ALGORITHMS
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-jwt" .Release.Name }}
                  key: algorithms
            - name: HHB_JWT_JWKS_URI
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-jwt" .Release.Name }}
                  key: jwksUri
            {{- with .Values.global }}{{- if .rabbitmq.enabled }}
            - name: HHB_RABBITMQ_HOST
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-rabbitmq" $.Release.Name }}
                  key: host
            - name: HHB_RABBITMQ_PORT
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-rabbitmq" $.Release.Name }}
                  key: port
            - name: HHB_RABBITMQ_USER
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-rabbitmq" $.Release.Name }}
                  key: username
            - name: HHB_RABBITMQ_PASS
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-rabbitmq" $.Release.Name }}
                  key: password
            {{- end }}{{- if .huishoudboekjeservice.enabled }}
            - name: HHB_HUISHOUDBOEKJE_SERVICE
              value: {{ printf "http://%s-%s" $.Release.Name "huishoudboekjeservice" | quote }}
            {{- end }}{{- if .rapportageservice.enabled }}
            - name: HHB_RAPPORTAGE_SERVICE
              value: {{ printf "http://%s-%s" $.Release.Name "rapportageservice" | quote }}
            {{- end }}{{- end }}
            {{- if .Values.extraEnvVars }}{{- toYaml .Values.extraEnvVars | nindent 12 }}{{- end }}
