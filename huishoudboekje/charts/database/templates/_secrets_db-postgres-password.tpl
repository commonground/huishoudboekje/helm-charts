{{/*
Fetch proper postgres password to use based on values or existing secret
*/}}

{{/*
Template function to lookup a value in a user provided secret and return it.
*/}}
{{- define "_getExistingDbpostgresSecretValue" -}}
{{- $resource := ((lookup "v1" "Secret" .context.Release.Namespace .context.Values.existingSecrets.credentials.name).data  | default dict) -}}
{{- printf "%s" (get $resource .info | b64dec) -}}
{{- end -}}

{{- define "db.postgres_password" -}}
    {{- coalesce (include "_getExistingDbpostgresSecretValue" (dict "context" . "info" .Values.existingSecrets.credentials.postgresPassword)) .Values.postgres_password -}}
{{- end -}}
