# huishoudboekje-helm

## 5.1.2

### Changes
- None

### Bugfixes
- [#85](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/86) hhbservice is not working properly
- hhbservice sub chart missing as dependency in parent Chart.yaml

## 5.1.1

### Changes
- Tweak default resource requests/limits for mesh service, userapi service, and partyservice.

### Bugfixes
- [#84](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/84) Mesh cannot communicate with hhbservicce

## 5.1.0

### Changes
- Update default HHB version to 2.3.0.
- Disable the postadressenservice by default.
- [#82](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/82) introduce HHB v2.3.0 compatibility:
  - Add `HHB_RAPPORTAGE_SERVICE` env var to the partyservice.
  - Add hhbservice.
  - Enable the userapi service by default.
- Remove example-values-complete.yaml as it did not add much value but was time-consuming to maintain.

### Bugfixes
- Add missing Redis environment variables to the userapi service.
  
## 5.0.0

### Changes
- Update default HHB version to 2.2.3.
- [#79](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/79) Make redis persistence optional.
- Update organisatieservice-to-partyservice-migration job.
- Update default resource requests/limits for mesh-, notification-, and party-, service.

### Bugfixes
- None

### Migration guide
- The values for the Redis service hve changed, `storageClassName` has moved to `persistence.pvc.storageClassName`.

## 4.3.5

### Changes
- [#77](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/77) Align database secret names so all
  services with a DB have a `POSTGRESQL_URL` and `POSTGRESQL_URL_LEGACY`, making automation in the data-importer easier.
- Update default HHB version to 2.2.2.
- Update DB sub chart livenessProbe and readinessProbe to be a bit faster
- Update DB sub chart to make PVC optional (may use emptyDir volume)

### Bugfixes
- Fix Fileservice and Logservice prepare-db jobs for included DB

## 4.3.4

### Changes
- None

### Bugfixes
- [#76](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/76) Fix partyservice service port name

## 4.3.3

### Changes
- Set default HHB version to 2.2.1.
- Disable organisatieservice by default.

### Bugfixes
- [#75](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/75) Fix log levels for .NET services.
- Fix accidental paste in partyservice secret.

## 4.3.2

### Changes
- None

### Bugfixes
- [#74](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/74) Fix missing partyservice DB secret.

## 4.3.1

### Changes
- Update default HHB version to 2.2.0.
- [#73](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/73) Add environment variables to the Mesh
  service and the partyservice.
- [#73](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/73) Partyservice is now enabled by default.

### Bugfixes
- Fix wrong JWKS URI if KeyCloak is enabled (now properly).

### Migration guide
- The Party service requires database connection credentials.

## 4.3.0

### Changes
- Update default HHB version to 2.1.5.
- Update default version of `prom/statsd-exporter` to `v0.27.2`.
- [#72](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/72) Add experimental support for the
  partyservice.

### Bugfixes
- Redis pod was not recreated when configuration changes on Helm release.
- Fix wrong JWKS URI if KeyCloak is enabled.
- Fix `imagePullPolicy` for the alarmenservice and the fileservice DB migration jobs.
- Uncomment `targetMemoryUtilizationPercentage` in values.yaml for the HPAs of most services, fixing a potential error.

## 4.2.5

### Changes
- Set default log level for Redis to `notice`.
- [#71](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/71) Allow specifying additional
  configuration for Redis, which is appended to the secret.

### Bugfixes
- None

## 4.2.4

### Changes
- Update default HHB version to 2.1.1

### Bugfixes
- [#70](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/70) Missing env var Banktransactieservice

## 4.2.3

### Changes
- None

### Bugfixes
- [#69](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/69) Wrong environment variables for HHBSBC and RPTSVC

## 4.2.2

### Changes
- None

### Bugfixes
- [#68](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/68) Missing env vars huishuidboekjeservice and rapportageservice

## 4.2.1

### Changes
- None

### Bugfixes
- [#67](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/67) Missing env var in Mesh service

## 4.2.0

### Changes
- Update default HHB version to v2.1.0
- [#66](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/66) Introduce compatibility with HHB v2.1.0
  - Introduce Fileservice
  - Use new Banktransactieservice
  - Enable Notificationservice by default

### Bugfixes
- Alarmenservice ServiceMonitor was using Logservice templating
- Fix Backend not prioritizing log level correctly (was favoring global setting over service specific setting).
- Fix missing log level configuration for Notificationservice
- Fix templating typo in userapi service

### Migration guide
- A new database is required for the Fileservice. This can either be added by the chart, of externally. In either case, credentials need to be configured.

## 4.1.0

### Changes
- Update default HHB version to 2.0.5

### Bugfixes
- [#65](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/65) Allow changing image repository of all images via values

### Migration guide
- No migration necessary

## 4.0.4

### Changes
- Update default HHB version to 2.0.2

### Bugfixes
- [#63](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/63) Alarmen service evaluate alarms cronjob connects to wrong RabbitMQ port

### Migration guide
- No migration necessary

## 4.0.3

### Changes
- None

### Bugfixes
- [#62](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/62) Add missing env vars to Alarmenservice
  and Logservice + add necessary templating

### Migration guide
- No migration necessary

## 4.0.2

### Changes
- None

### Bugfixes
- [#61](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/61) Fix typo in Alarmenservice
  evaluate-alarms cronJob

### Migration guide
- No migration necessary

## 4.0.1

### Changes
- Update READMEs to clarify one-time DB migration scripts need to run after the HHB v2.0.0 update, not before

### Bugfixes
- [#60](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/60) Add missing dependency for one-time DB
  migration scripts
- [#60](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/60) Fix accidental unnoticed paste of test
  in _helpers.tpl
- [#60](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/60) Remove image tag dependency from
  _helpers.tpl
- [#60](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/60) Add POSTGRESQL_URL_LEGACY key to
  logservice DB secret because the one-time DB migration requires the Python syntax

### Migration guide
- No migration necessary

## 4.0.0

### Changes
- [#59](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/59) Introduce HHB v2.0.0 compatibility
  - Update Alarmenservice and Logservice sub charts for new .NET based service
    - Update ports
    - Update env vars
    - Remove StatsD exporter from Logservice
    - Remove log level configuration
  - Add RabbitMQ sub chart
  - Add GraphQL Mesh sub chart
  - Add UserAPI sub chart
  - Add Notificationservice sub chart
  - Completely remove Unleashservice
  - Replace Python check-alarms cronjob with RabbitMQ message publisher
  - Implement one-time DB migration tool/script for Alarmenservice and Logservice
  - Update regular DB migration jobs for Alarmenservice and Logservice
  - Add RabbitMQ credentials to backend environment variables
- Rename "check-alarms" cronjob to "evaluate-alarms", move it the Alarmenservice sub chart and add more configurable
  options
- Add Prometheus metrics collection to new Alarmenservice, with optional ServiceMonitor
- Update default HHB version to 2.0.1
- Update image version of prom/statsd-exporter to v0.26.1
- Tweak default resource requests/limits for various services
- set `enableServiceLinks` and `autoMountServiceAccountToken` to false on all services
- Remove option to configure service port for Alarmenservice and Logservice

### Bugfixes
- KeyCloak sub chart: add existingSecret for authentication empty values to prevent an error if they are never filled

### Migration guide
**This release introduces breaking changes is NOT compatible with any previous version of HHB (<2.0.0).**
- One time migration script for the Alarmenservice and Logservice must be run prior to upgrading to HHB 2.0.0. HHB must
  be upgraded to v1.12.7 first if not on that version already.
- Credentials for RabbitMQ (at least a password) must be supplied (otherwise default password will be used).
- The Signalenservice and Unleashservcie has been removed, database for Signalenservice can be deleted.
- Prometheus EnableServiceMonitor Helm value changed location (indentation).

## 3.4.3

### Changes
- [#58](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/58) Set gunicorn_worker_timeout to 0 by default
- Update default HHB version to 1.12.2.

### Bugfixes
- None

### Contributors for this release
@tomwiggers

### Migration guide
- Extra environment variable `GUNICORN_WORKER_TIMEOUT=0` is not needed anymore and can be removed from values if it was
  configured. It is not included in the Helm chart itself.

## 3.4.2

### Changes
- Configure database connection SSL mode and set it to `prefer`.

### Bugfixes
- [#56](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/56) Postadressenservice may throw certificate error when seeding test data into a database over a connection using 
  self-signed certificates.

### Contributors for this release
@tomwiggers

### Migration guide
- No migration necessary.

## 3.4.1

### Changes
- None

### Bugfixes
- Release 3.4.0 contained a typo in the `deployment.yaml` file of the banktransactieservice causing an error.

### Contributors for this release
@tomwiggers

### Migration guide
- No migration necessary.

## 3.4.0
WARNING: Please disregard this release and move to version 3.3.1.

### Changes
- Rename Helm hooks (+ k8s jobs) to be more in line with each other.
- Update v3.3.2 changelog
- [#55](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/55) Implement new way to load test data into HHB

### Bugfixes
- None

### Contributors for this release
@tomwiggers

### Migration guide
- No migration necessary.

## 3.3.2

### Changes
- None

### Bugfixes
- [#54](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/54) Redis cannot save temp RDB files when using ROFS

### Contributors for this release
@tomwiggers

### Migration guide
- None

## 3.3.1

### Changes
- None

### Changes compared to version 3.2.0
- [#51](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/51) Huishoudboekje 1.11.0 compatibility
  - Include Redis caching server.
- Update default HHB version to 1.11.0.

### Bugfixes
- [#52](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/52) Documentation in example-values-complete incorrect
- [#53](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/53) Rework Redis implementation

### Contributors for this release
@tomwiggers

### Migration guide
- Redis credentials have been moved to global values.
- Completely backwards compatible with Helm chart version 3.2.0 but not with 3.3.0. Compared to version 3.2.0, the
  following needs to be done:
  - Add Redis credentials (at least a password) either to `.Values.global.redis.password` or using external secret.
  - If desired, disable the included Redis server (`.Values.global.redis.enabled`) and configure external Redis server.
  - Refer to [`example-values-complete.yaml`](./example-values-complete.yaml) for more information and documentation.

## 3.3.0
WARNING: Please disregard this release and move to version 3.3.1.

### Changes
- [#51](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/51) Huishoudboekje 1.11.0 compatibility
  - Include Redis caching server.
- Update default HHB version to 1.11.0.

### Bugfixes
- None

### Contributors for this release
@tomwiggers

### Migration guide
- Add Redis credentials to values of authservice. Either using plain text values or using existing secret.
  The username and password provided to the authservice for Redis will be used to configure Redis.

## 3.2.0

### Changes
- [#48](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/48) Include option to install Prometheus 
  ServiceMonitor (CRD)
- [#50](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/50) Support enabling GraphQL Introspection

### Bugfixes
- [#49](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/49) Service definitions have Prometheus
  metrics port included even if metrics are turned off

### Contributors for this release
@tomwiggers

### Migration guide
- No migration needed

## 3.1.0

### Changes
- [#47](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/47) Introduce StatsD support with optional
  Prometheus exporter.

### Bugfixes
- None

### Contributors for this release
@tomwiggers

### Migration guide
- No migration needed.

## 3.0.4

### Changes
- [#45](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/45) Support HHB 1.10.1 and set default HHB
  version to 1.10.1.
- [#45](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/45) Make backend env vars
  `INTERNAL_CONNECTION_TIMEOUT` and `INTERNAL_READ_TIMEOUT` optional and configurable.

### Bugfixes
- [#46](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/46) Keycloak setup tool does not set valid
  post-logout redirect URL

### Contributors for this release
@Justin-Booij @tomwiggers

### Migration guide
- JWT Algorithms can now be defined. By default HS256 and RS256 are supported. If others are required, define them in
  `.Values.global.jwt.algorithms`.

## 3.0.3

### Changes
- None

### Bugfixes
- [#42](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/42) Improve documentation regarding KeyCloak
  client secret
- [#43](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/43) Allow database password for postgres
  user of included DB to specified via secret
- [#44](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/44) JWT validity period does not to get set
  correctly when not defined via values/secret

### Contributors for this release
@tomwiggers

### Migration guide
- No migration necessary.

## 3.0.2

### Changes
- None

### Bugfixes
- [#41](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/41) KeyCloak client secret is not handled
  correctly when KeyCloak is used

### Contributors for this release
@tomwiggers

### Migration guide
- No migration necessary.

## 3.0.1

### Changes
- [#39](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/39) Change default CPU limit for
  rapportageservice to 1500m.

### Bugfixes
- [#40](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/40) Allow setting KeyCloak auth and users
  via existing secrets
- [#38](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/38) Fix adding additional environment vars
  to services.

### Migration guide
- No migration necessary.

### Contributors for this release
@tomwiggers

## 3.0.0

### Changes
- [#26](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/26) Allow usage of self-supplied secrets
- Related to #26: Remove global database settings and move all OIDC and JWT values to global values.
- Update default HHB version 1.9.8.
- Move test-db-connection job from its own Helm hook to an init container of get-db-timestamp hook.
- Set default log level back to `INFO`.
- The password for the postgres user of the included DB is now defined using the value `postgres_password` as opposed to
  `internal_db_password`.
- Remove useless values from backend and keycloak manifests.
- Remove legacy OpenShift Container Platform (OCP) 3 reference from deployment manifests of various services.

### Bugfixes
- Fix typo in Postadressenservice deployment.
- Fix bug in KeyCloak DB setup when both included KeyCloak and DB are enabled.
- [#36](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/36) Possible incorrect HHB version in labels of manifests

### Migration guide
- Global database values are not used anymore, from this version of DB settings must be defined per service and values
  and secrets are completely ignored if included DB is enabled.
- JWT and OIDC values have been moved from KeyCloak and authservice values to global values.
- If using the included PostgreSQL DB, the password for the postgres user must be moved from `internal_db_password` to
  `postgres_password` in values files.

### Contributors for this release
@tomwiggers

## 2.3.0

### Changes
- [#37](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/37) Huishoudboekje 1.9.3 compatibility
- Update default HHB version to 1.9.3.
- Update compatibility table on README.

### Bugfixes
- Backend: do not include `UNLEASHSERVICE_URL` env var if the Unleash service is not enabled.

### Migration guide
- The backend now uses the environment variable `USE_GRAPHIQL` that will determine the use of the graphiql interface.
  This will default to "0" if not set. For production environments this should be set to "0"!

### Contributors for this release
@tomwiggers

## 2.2.2

### Changes
- Update Helm chart compatibility table.

### Bugfixes
- [#35](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/35) Service database migration pods may use diffrent image version than application pod

### Migration guide
- No migration necessary.

### Contributors for this release
@douwemeulenbroek @tomwiggers

## 2.2.1

### Changes
- Update default HHB version to 1.9.0.
- Update compatibility table on README.

### Bugfixes
- [#33](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/33) Check alarms cronjob is missing some environment variables
- [#34](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/34) Backend service has wrong value in UNLEASHSERVICE_URL env var

### Migration guide
- No migration necessary.

### Contributors for this release
@tomwiggers

## 2.2.0

### Changes
- [#30](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/30) Add support for additional user-supplied annotations on ingress/Route objects
- Change default HHB version 1.8.3.

### Bugfixes
- None

### Migration guide
- No migration necessary.

### Contributors for this release
@tomwiggers

## 2.1.1

### Changes
- Clarified (ab)use of Semver in [README](./README.md).
- Change default HHB version 1.8.1.

### Bugfixes
- [#27](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/27): Check alarms cron job may lack image tag.
- Backend was missing required environment variable for the rapportageservice to work.

### Migration guide
- For HHB versions < 1.8.1, the rapportage service must be explicitly disabled using values as in earlier versions the
  rapportage service does not exist.

### Contributors for this release
@tomwiggers

## 2.1.0

### Changes
- Add support for the new rapportageservice (default off).
- Update JSON schema file for Helm values.
- Change default log level for all services to `WARNING`.

### Bugfixes
- Update version of database sub chart dependency in the parent chart's `Chart.yaml` file, this fixes usage of built-in
  database sub chart.
- Amend contributors for previous release.

### Migration guide
- No migration necessary.

### Contributors for this release
@tomwiggers

## 2.0.1

### Changes
- Use base64 encoding for most secrets instead of plain text.

### Bugfixes
- [#23](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/23) BUG: global image tag override is not
  used.
- Correct CHANGELOG for 2.0.0 release.

### Migration guide
- The `appVersion` Helm chart property is now used again to determine default Huishoudboekje service image version. This
  does not count for the database and keycloak sub charts. This change partially reverts the change in 2.0.0 meaning the
values file from 1.2.0 are still compatible (or 1.4.0 if KeyCloak is used with users configured).

### Contributors for this release
@tomwiggers
 
## 2.0.0

### Changes
- The Helm chart versioning no longer follows the Huishoudboekje version but is seperately versioned. See 'Versioning'
  in the [README](./README.md).
- Use Huishoudboekje 1.7.0 by default.

### Bugfixes
- Fix backend error caused by wrong environment variable containing the URL to the Unleash service.

### Migration guide
- The `appVersion` Helm chart property is no longer used to determine Huishoudboekje version for install or upgrade. The
  value `global.image.tag` must now be used. This can still be overriden on a per-sub chart basis using the value
  `subchart.image.tag`. The KeyCloak and database sub chart do not use the `global.image.tag` value and must overriden
  individually if desired.

### Contributors for this release
@tomwiggers

## 1.4.0

### Changes
- Use Huishoudboekje 1.4.0 by default.
- Allow additional environment variables to be added to every service.
- Alarmen and signalen are officially released, these services are now enabled by default.
- Auto scaling (HPA) is disabled by default for all services.
- KeyCloak has been upgraded to 19.0.3.
- Default resource requests and limits have been increased for the backend service.
- Added an icon for the chart (same as the one on GitLab).

### Bugfixes
None.

### Migration guide
- The KeyCloak setup tool now requires a new format for users. Please refer to the
  [example values file](./example-values-complete.yaml).
No migration necessary. Values of previous versions down to 1.2.0 are compatible.

### Contributors for this release
@tomwiggers

## 1.3.0-1

### Changes
- Updated example values file.
- Use new version of keycloak setup tool which allows an empty list of users to import.

### Bugfixes
- Fix DB migration of several services if external DB is used, it broke due to having `readOnlyFilesystem` on by
default.

### Migration guide
No migration necessary. Values of previous versions down to 1.2.0 are compatible.

### Contributors for this release
@tomwiggers

## 1.3.0

### Changes
- All services except the included DB and KeyCloak now use a read-only root filesystem. Some volumes for temporary logs
  and caches have been added to make this possible.
- All services nor run as non-root.
- Added a configurable cronjob to check for alarms. By default it runs at 03:00 every night.
- The python based services now use a different command to migrate their DBs.

### Bugfixes
None.

### Migration guide
- Values have been added to enable or disable the cronjob to check alarms and set its schedule. Values of previous
  versions up to 1.2.0 are compatible.

### Contributors for this release
@tomwiggers

## 1.2.3

### Changes
- For all initContainers that did not have it yet: added securityContext with `runAsNonRoot: true`, `allowPrivilegeEscalation: false`, and, if the service allows for it, `readOnlyRootFilesystem` set to `true`
- Add optional volumemounts for caches to the services using npm and prisma to prepare these services for `readOnlyRootFilesystem`.
- Increased resource limits for the Alarmenservice, Postadressenservice and Signalenservice.
- Add volumemount to the backend to allow a writable `tmp` directory.

### Bugfixes
- The Alarmenservice and the Postadressenservice cannot function right now with `readOnlyRootFilesystem` set to `true`. Set it to `false`.
- Bugfix in name of the database secret reference for the Alarmenservice wait-for-db init container.

### Migration guide
No migration necessary. Values of 1.2.1-1, 1.2.1 and 1.2.0 are compatible.

### Contributors for this release
@koenbrouwer, @douwemeulenbroek, @tomwiggers

## 1.2.1-1

### Changes
- Remove unneeded KeyCloak DB preparation job

### Bugfixes
- Fix typos in various locations
- 02f8bf2: Rename all jobs to truncate the names at max 63 characters
- Fix the Helm hook-weight of the KeyCloak secrets manifest

### Migration guide
No migration necessary. This is only a bugfix release. Values of 1.2.0 and 1.2.1 are compatible with 1.2.1-1.

## 1.2.1
Compatible with Huishoudboekje 1.2.1+ only.

### Changes
- 2788594: Update included KeyCloak to default version 18.0.2
- 646d096: Updated internal environment variables for database urls to be compatible with Huishoudboekje 1.2.1.
- a8d3c7b: Security fixes: for all containers, drop ALL capabilities and do not allow privilege escalation
- ad0fa5b: Rename and improve keycloak setup job
- 2788594: Rename all secrets and configmaps to names resolvable by other sub charts
- Add initial values.schema.json file to force correct values for the charts

### Bugfixes
- ad0fa5b: Repair prepare-db init container of alarmenservice
- da551b7: Bugfix: Signalenservice does not start because of readOnlyRootFilesystem (fix: make it writable...)
- ad0fa5b: Allow unleashservice to write to its filesystem and give it longer to start up before killing it
- c9dcf18: Fix database_url environment variable for signsalenservice init container amd convert internal database to
  statefulset
- ad0fa5b: Increase memory for the signalenservice
- a07e592: Bugfix: Fix signalen service database secret name
- d4e9493: Bugfix: Fix secrets manifest checksum inclusion in the deployments of the alarmenservice and signalenservice
- 2788594: Move checksum annotations to correct locations
- 565e49c: Bugfix: Fix image tag in alarmenservice deployment
- a57939a: Bugfix: Fix backendburgers deployment by adding volumes to allow running with readOnlyFileSystem and fix
  prepare-db init container args
  Bugfix: Fix signalenservice deployment by adding volumes to allow running with readOnlyFileSystem
- ad0fa5b: Fix typo in postadressenservice

### Migration guide
No migration necessary. Values of 1.2.0 are compatible with 1.2.1.

### Contributors for this release
@koenbrouwer, @tomwiggers

## 1.2.0
Compatible with Huishoudboekje 1.2.0.

### Minor Changes
- 743fd27: Fix Keycloak installation with scripts.
- a4e946d: Frontend, including internal Keycloak if enabled, must now be reachable via HTTPS. Also use internal Keycloak
  service name where possible.
- 6fe9513: Introduce autoscaling/v2 API for HPAs and use that if Kubernetes supports it. Should remove the deprecation
  warnings about the autoscaling/v2beta1 API.
- 5e3961b: A password for the Keycloak admin user is no longer automatically generated if none was provided.
- 6186801: Use new Keycloak setup tool, which also allows easier setup of users.
- Support Huishoudboekje 1.2.0: introduce authservice and move OIDC settings there.

### Patch Changes
- 9621dce: Remove Storybook (only for development, and the Helm charts are not used for development).
- ba803fc: Keycloak now listens on /kc instead of /auth (the new authservice now listens on /auth).
- 6c0f2b5: Add Minikube support.
- 9127963: The storage class name for the database is now optional and can be provided using values. By default there is
  no storage class name specification.
- bca303d: Do not specify storage class for the database PVC when platform is OCP4.
- ba803fc: Log level is now configurable per service and globally.
- 0e24a5e: Update example complete values.
- 2969fc5: Remove OCP3 compatibility (only a patch level change as OCP3 is not used anymore, there is no impact).
  Use Helm Capabalitities check to determine if API version is available instead of using the platform value.
  Remove platform value (this is a backwards compatible change; the platform is now simply unused)
  Update example-values-complete.yaml.
- ba803fc: The image tags and image pull policies of the Huishoudboekje services are now per service and globally
 configurable (does not apply to the database, keycloak and to all init containers).
- bf6c5b1: Make connection scheme to connect to the OIDC endpoints and Keycloak (separately) configurable, default to
 https (backwards compatible change).

### Migration guide
- [_*BREAKING*_] The OIDC values need to be moved from the backend to the new authservice.
  - The endpoint URLs are now redundant, they are automatically determined in Huishoudboekje 1.2.0.
- [_*BREAKING*_] KeyCloak now uses a new script to automatically add the Huishoudboekje realm, the OIDC information and any users.
  - An admin password for KeyCloak is now required. It is no longer automatically generated.
- [_*BREAKING*_] The Helm Charts are no longer compatible with Red Hat OpenShift version 3.
- The "platform" global value is now redundant.
- The "profile" value in the backend is now redundant.

See the example values file for information and documentation of each value.

### Contributors for this release
@koenbrouwer, @opvolger, @tomwiggers

## 1.0.0

### Major Changes
- First public release
