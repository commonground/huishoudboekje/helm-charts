{{/*
Template function to translate log levels to levels which work with .NET services. Takes a "regular" log level as input.
*/}}
{{- define "getDotNetLogLevel" -}}
{{- if eq . "CRITICAL" -}}
  {{- printf "Critical" -}}
{{- else if eq . "ERROR" -}}
  {{- printf "Error" -}}
{{- else if eq . "WARNING" -}}
  {{- printf "Warning" -}}
{{- else if eq . "INFO" -}}
  {{- printf "Information" -}}
{{- else if eq . "DEBUG" -}}
  {{- printf "Debug" -}}
{{- else if eq . "NOTSET" -}}
  {{- printf "None" -}}
{{- end -}}
{{- end -}}
