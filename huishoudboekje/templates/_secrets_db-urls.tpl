{{/*
Generate DB connection URLs per service based on provided values or defaults.
*/}}

{{/*
Template function to lookup a value in a user provided DB secret and return it.
*/}}
{{- define "_getExistingDbSecretValue" -}}
{{- $resource := ((lookup "v1" "Secret" .context.Release.Namespace .context.Values.existingSecrets.database.name).data  | default dict) -}}
{{- printf "%s" (get $resource .info | b64dec) -}}
{{- end -}}

{{/*
Below the DB connection properties are gathered. Existing secrets have preference over values. Host and port settings
are ignored if included DB is enabled.
*/}}

{{- define "db.host" -}}
{{- if .Values.global.database.enabled -}}
  {{- (printf "%s-%s" $.Release.Name "database") -}}
{{- else -}}
    {{- coalesce (include "_getExistingDbSecretValue" (dict "context" . "info" .Values.existingSecrets.database.host)) .Values.database.host -}}
{{- end -}}{{- end -}}

{{- define "db.port" -}}
{{- if .Values.global.database.enabled -}}
  {{- "5432" -}}
{{- else -}}
  {{- coalesce (include "_getExistingDbSecretValue" (dict "context" . "info" .Values.existingSecrets.database.port)) .Values.database.port -}}
{{- end -}}{{- end -}}

{{- define "db.username" -}}
  {{- coalesce (include "_getExistingDbSecretValue" (dict "context" . "info" .Values.existingSecrets.database.username)) .Values.database.username -}}
{{- end -}}

{{- define "db.password" -}}
  {{- coalesce (include "_getExistingDbSecretValue" (dict "context" . "info" .Values.existingSecrets.database.password)) .Values.database.password -}}
{{- end -}}

{{- define "db.name" -}}
  {{- coalesce (include "_getExistingDbSecretValue" (dict "context" . "info" .Values.existingSecrets.database.dbName)) .Values.database.dbName -}}
{{- end -}}

{{/*
Generate database connection URLs based on host, port, username, password and DB name, for legacy Python services and
newer .NET services.
*/}}
{{- define "db.url" -}}
{{/* .NET */}}
{{- printf "Host=%s;Database=%s;Port=%s;Username=%s;Password=%s" (include "db.host" .) (include "db.name" .) (include "db.port" .) (include "db.username" .) (include "db.password" .) -}}
{{- end -}}
{{- define "db.url_legacy" -}}
{{/* Python */}}
{{- printf "postgresql://%s:%s@%s:%s/%s?sslmode=prefer" (include "db.username" .) (include "db.password" .) (include "db.host" .) (include "db.port" .) (include "db.name" .) -}}
{{- end -}}
