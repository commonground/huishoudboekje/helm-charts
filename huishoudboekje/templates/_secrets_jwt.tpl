{{- /*
Generate JWT templates containing the information either from user supplied existing/external secrets or from values.

External/existing secrets take precedence over values.
*/ -}}

{{- /*
Template function to lookup a value in a user provided JWT secret and return it.
*/ -}}
{{- define "_getExistingJwtSecretValue" -}}
{{- $resource := ((lookup "v1" "Secret" .context.Release.Namespace .context.Values.global.existingSecrets.jwt.name).data | default dict) -}}
{{- printf "%s" (get $resource .info | b64dec) -}}
{{- end -}}

{{- define "jwt.issuer" -}}
{{- if .Values.global.keycloak.enabled -}}
    {{- coalesce (include "_getExistingJwtSecretValue" (dict "context" . "info" .Values.global.existingSecrets.jwt.issuer)) .Values.global.jwt.issuer (printf "https://%s/kc/realms/%s" $.Values.global.dns_domain $.Release.Name) -}}
{{- else -}}
    {{- coalesce (include "_getExistingJwtSecretValue" (dict "context" . "info" .Values.global.existingSecrets.jwt.issuer)) .Values.global.jwt.issuer (printf "%s" $.Release.Name) -}}
{{- end -}}
{{- end -}}

{{- define "jwt.audience" -}}
{{- if .Values.global.keycloak.enabled -}}
  {{- coalesce (include "_getExistingJwtSecretValue" (dict "context" . "info" .Values.global.existingSecrets.jwt.audience)) .Values.global.jwt.audience "account" -}}
{{- else -}}
  {{- coalesce (include "_getExistingJwtSecretValue" (dict "context" . "info" .Values.global.existingSecrets.jwt.audience)) .Values.global.jwt.audience (printf "https://%s" .Values.global.dns_domain) -}}
{{- end -}}
{{- end -}}

{{- define "jwt.secret" -}}
{{- /* Generate jwt secret if it is not defined using 16 tokens 0-9, a-z, A-Z. */ -}}
{{- coalesce (include "_getExistingJwtSecretValue" (dict "context" . "info" .Values.global.existingSecrets.jwt.secret)) .Values.global.jwt.secret (printf "%s" (randAlphaNum 16)) -}}
{{- end -}}

{{- define "jwt.validityPeriod" -}}
{{- coalesce (include "_getExistingJwtSecretValue" (dict "context" . "info" .Values.global.existingSecrets.jwt.validityPeriod)) .Values.global.jwt.validityPeriod -}}
{{- end -}}

{{- define "jwt.algorithms" -}}
{{- coalesce (include "_getExistingJwtSecretValue" (dict "context" . "info" .Values.global.existingSecrets.jwt.algorithms)) .Values.global.jwt.algorithms -}}
{{- end -}}

{{- define "jwt.jwksUri" -}}
{{- if .Values.global.keycloak.enabled -}}
  {{- coalesce (include "_getExistingJwtSecretValue" (dict "context" . "info" .Values.global.existingSecrets.jwt.jwksUri)) .Values.global.jwt.jwksUri (printf "https://%s/kc/realms/%s/protocol/openid-connect/certs" $.Values.global.dns_domain $.Release.Name) -}}
{{- else -}}
  {{- coalesce (include "_getExistingJwtSecretValue" (dict "context" . "info" .Values.global.existingSecrets.jwt.jwksUri)) .Values.global.jwt.jwksUri -}}
{{- end -}}
{{- end -}}
