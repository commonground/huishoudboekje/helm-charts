{{/*
Generate OIDC templates containing the information either from KeyCloak if installed or user user supplied information.
The user supplied information can either come from values directly or from external/existing secrets.

External/existing secrets take precedence over values.
*/}}

{{/*
Template function to lookup a value in a user provided OIDC secret and return it.
*/}}
{{- define "_getExistingOidcSecretValue" -}}
{{- $resource := ((lookup "v1" "Secret" .context.Release.Namespace .context.Values.global.existingSecrets.oidc.name).data | default dict) -}}
{{- printf "%s" (get $resource .info | b64dec) -}}
{{- end -}}

{{- define "oidc.issuer" -}}
{{- if .Values.global.keycloak.enabled -}}
  {{- printf "https://%s/kc/realms/%s" .Values.global.dns_domain $.Release.Name }}
{{- else -}}
  {{- coalesce (include "_getExistingOidcSecretValue" (dict "context" . "info" .Values.global.existingSecrets.oidc.issuer)) .Values.global.oidc.issuer -}}
{{- end -}}{{- end -}}

{{- define "oidc.clientId" -}}
{{- if .Values.global.keycloak.enabled -}}
  {{- printf $.Release.Name -}}
{{- else -}}
  {{- coalesce (include "_getExistingOidcSecretValue" (dict "context" . "info" .Values.global.existingSecrets.oidc.clientId)) .Values.global.oidc.clientId -}}
{{- end -}}{{- end -}}

{{- define "oidc.clientSecret" -}}
  {{- coalesce (include "_getExistingOidcSecretValue" (dict "context" . "info" .Values.global.existingSecrets.oidc.clientSecret)) .Values.global.oidc.clientSecret -}}
{{- end -}}
