{{/*
Generate templates to determine Redis URL.
*/}}

{{/*
Template function to lookup a value in a user provided secret and return it.
*/}}
{{- define "_getExistingRedisUrlSecretValue" -}}
{{- $resource := ((lookup "v1" "Secret" .context.Release.Namespace .context.Values.global.existingSecrets.redis.name).data | default dict) -}}
{{- printf "%s" (get $resource .info | b64dec) -}}
{{- end -}}

{{- define "redis.username" -}}
  {{- coalesce (include "_getExistingRedisUrlSecretValue" (dict "context" . "info" .Values.global.existingSecrets.redis.username)) .Values.global.redis.username -}}
{{- end -}}

{{- define "redis.password" -}}
  {{- coalesce (include "_getExistingRedisUrlSecretValue" (dict "context" . "info" .Values.global.existingSecrets.redis.password)) .Values.global.redis.password -}}
{{- end -}}

{{- define "redis.host" -}}
{{- if .Values.global.redis.enabled -}}
  {{- (printf "%s-%s" $.Release.Name "redis") -}}
{{- else -}}
  {{- coalesce (include "_getExistingRedisUrlSecretValue" (dict "context" . "info" .Values.global.existingSecrets.redis.host)) .Values.global.redis.host -}}
{{- end -}}{{- end -}}

{{- define "redis.port" -}}
{{- if .Values.global.redis.enabled -}}
  {{- "6379" -}}
{{- else -}}
  {{- coalesce (include "_getExistingRedisUrlSecretValue" (dict "context" . "info" .Values.global.existingSecrets.redis.port)) .Values.global.redis.port -}}
{{- end -}}{{- end -}}

{{/*
Generate Redis connection URL based on host, port, username and password.
*/}}
{{- define "redis.url" -}}
{{- printf "redis://%s:%s@%s:%s" (include "redis.username" .) (include "redis.password" .) (include "redis.host" .) (include "redis.port" .) -}}
{{- end -}}
