{{/*
Generate templates to determine RabbitMQ credentials.
*/}}

{{/*
Template function to lookup a value in a user provided secret and return it.
*/}}
{{- define "_getExistingRabbitmqUrlSecretValue" -}}
{{- $resource := ((lookup "v1" "Secret" .context.Release.Namespace .context.Values.global.existingSecrets.rabbitmq.name).data | default dict) -}}
{{- printf "%s" (get $resource .info | b64dec) -}}
{{- end -}}

{{- define "rabbitmq.username" -}}
  {{- coalesce (include "_getExistingRabbitmqUrlSecretValue" (dict "context" . "info" .Values.global.existingSecrets.rabbitmq.username)) .Values.global.rabbitmq.username -}}
{{- end -}}

{{- define "rabbitmq.password" -}}
  {{- coalesce (include "_getExistingRabbitmqUrlSecretValue" (dict "context" . "info" .Values.global.existingSecrets.rabbitmq.password)) .Values.global.rabbitmq.password -}}
{{- end -}}

{{- define "rabbitmq.host" -}}
{{- if .Values.global.rabbitmq.enabled -}}
  {{- (printf "%s-%s" $.Release.Name "rabbitmq") -}}
{{- else -}}
  {{- coalesce (include "_getExistingRabbitmqUrlSecretValue" (dict "context" . "info" .Values.global.existingSecrets.rabbitmq.host)) .Values.global.rabbitmq.host -}}
{{- end -}}{{- end -}}

{{- define "rabbitmq.port" -}}
{{- if .Values.global.rabbitmq.enabled -}}
  {{- "5672" -}}
{{- else -}}
  {{- coalesce (include "_getExistingRabbitmqUrlSecretValue" (dict "context" . "info" .Values.global.existingSecrets.rabbitmq.port)) .Values.global.rabbitmq.port -}}
{{- end -}}{{- end -}}

{{- define "rabbitmq.managementPort" -}}
{{- if .Values.global.rabbitmq.enabled -}}
  {{- "15672" -}}
{{- else -}}
  {{- coalesce (include "_getExistingRabbitmqUrlSecretValue" (dict "context" . "info" .Values.global.existingSecrets.rabbitmq.managementPort)) .Values.global.rabbitmq.managementPort -}}
{{- end -}}{{- end -}}
